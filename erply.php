<?php
include 'config_test.php'; //config_php - production DB; config_test.php - test DB
include 'func.php';

session_start();

// include ERPLY API class
include("EAPI.class.php");

// Initialise class
$api = new EAPI();

// Configuration settings
$api->clientCode = $client_code;
$api->username = $erply_user;
$api->password = $erply_pass;
$api->url = "https://".$api->clientCode.".erply.com/api/";

$Product_ID = 62;
$price = 100;
$xml_price = calculate_price($price);

/** Create a new customer group**/
$inputParameters = array(
	"groupID" => "1",
	"vatrateID" => "1",
	"code" => "TODO",
	"supplierCode" => "TODO",
	"active" => "1",
	"displayedInWebshop" => "1",
	"name" => "TODO",
	"description" => "TODO",
	"priceWithVAT" => "$xml_price",
	"cost" => "$price",
	"manufacturerName" => "",
);
$result = $api->sendRequest("saveProduct", $inputParameters);

// Default output format is JSON, so we'll decode it into a PHP array
$output = json_decode($result, true);

print "<pre>";
print_r($output);
print "</pre>";





// Get client groups from API
/** No input parameters are needed**/
$result = $api->sendRequest("getVatRates", array());

// Default output format is JSON, so we'll decode it into a PHP array
$output = json_decode($result, true);

print "<pre>";
print_r($output);
print "</pre>";

function get_product_group_id($category) {
	if ($category == 'Card Technology') $group_id = 102;
	if ($category == 'ID Technology') $group_id = 103;
	if ($category == 'Barcode Readers') $group_id = 104;
	if ($category == 'Monitors (touch)') $group_id = 105;
	if ($category == 'Cash Drawers') $group_id = 106;
	if ($category == 'POS Keyboards') $group_id = 107;
	if ($category == 'Currency Testers/Counters') $group_id = 108;
	if ($category == 'POS Systems / PCs') $group_id = 109;
	if ($category == 'Accessories') $group_id = 111;
	if ($category == 'POS Printers') $group_id = 112;
	if ($category == 'Label Printers') $group_id = 113;
	if ($category == 'Customer Displays') $group_id = 114;
	if ($category == 'Mobile Terminals') $group_id = 115;
	if ($category == 'Notebooks (rugged)') $group_id = 116;
	if ($category == 'VESA Mount') $group_id = 117;
	if ($category == 'Kiosk Systems') $group_id = 118;
	if ($category == 'WLAN Infrastructure') $group_id = 119;
	else $group_id = 110;
	return $group_id;
}

?>