<?php
$import = 0; //first have it 0 and check if all looks good and then set it 1 and actually import to DB
include 'config.php'; //config_php - production DB; config_test.php - test DB

$handle = fopen("downloaded.csv", "r");
$mysqli = new mysqli($servername, $username, $password, $dbname);
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

while (($data = fgetcsv($handle, 1000, ';', '"')) !== false) {
	$product_nr = $data[0];
	$category = $data[1];
	$sub_category = $data[2];
	$main_product = $data[3];
	$stock = $data[4];
	$packing_unit = $data[5];
	$product_name = $data[6];
	$product_description = $data[7];
	$datasheet = $data[8];
	$warranty = $data[9];
	$price =  $data[10];
	$Product_ID = $data[11];
	$picture = $data[12];
	$vendor = $data[13];
	$date_price_change = $data[15];
	$weight = $data[16];
	$pre_exchange = $data[18];
	$type = $data[19];
	$fullcover = $data[20];
	
	if (empty($sub_category)) $sub_category = $category; //if there's no subcategory for this product then it belongs directly under main category
	
	$sql = "SELECT original_number FROM products WHERE original_number LIKE '$Product_ID%'"; //find the product with current original_number
	$products = $mysqli->query($sql);
	if($products->num_rows == 0) { //add product to database as it doesn't exist
		$sql = "SELECT name FROM main_products WHERE name='$main_product'"; //find product's main product
		$mainproducts = $mysqli->query($sql);
		if($mainproducts->num_rows == 0) { //add main product to database as it doesn't exist
			$sql = "SELECT id FROM categories WHERE name='$sub_category'"; //get the id of the subcategory of this product
			$subcategories = $mysqli->query($sql);
			
			if($subcategories->num_rows == 0) { //subcategory is missing and we have to check for main category
				$sql = "SELECT id FROM categories WHERE name='$category'"; //get the id of the main category of this product
				$categories = $mysqli->query($sql);
				if($categories->num_rows == 0) { //main category is missing
					echo $category . '<font color=red size=12>MAIN CATEGORY MISSING!</font> -- ';
				}
				echo $sub_category . '<font color=red>SUBCATEGORY MISSING! (' . $category . ')</font> -- ';
			}
			
			$row = $subcategories->fetch_assoc();
			$category_id = $row['id'];
				
			$sql = "INSERT INTO main_products (name, manufacturer, picture, datasheet, subcategory_id) 
					VALUES ('$main_product', '$vendor', '$picture', '$datasheet', '$category_id')"; //add main product
			if ($import = 1) $add_main = $mysqli->query($sql);

			echo $main_product . ' -- NO MAIN PRODUCT! -- ';
		}
		
		$sql = "INSERT INTO products (number, name, quantity, packing_unit, description, warranty, weight, pre_exchange, date_price_change, price, original_number, type, fullcover, main_product_name, source) VALUES ('$product_nr', '$product_name', '$stock', '$packing_unit', '$product_description', '$warranty', '$weight', '$pre_exchange', '$date_price_change', '$price', '$Product_ID', '$type', '$fullcover', '$main_product', 'JAR')";
		if ($import = 1) $add_product = $mysqli->query($sql);
		
		echo $product_name . ' (' . $Product_ID . ') <font color=blue>DOES NOT EXIST!!!</font><br>';
	}
	else echo $Product_ID . ' EXISTS<br>';
}

$mysqli->close();
fclose($handle);