<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</head>
<body>
<?php
header('Content-Type: text/html; charset=utf-8');
include 'config_test.php'; //config_php - production DB; config_test.php - test DB

//XML header
$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\" ?><Pricelist xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://www.hinnavaatlus.ee/xml_hinnakiri.xsd\"></Pricelist>"); //hinnavaatlus.xml
$Gxml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />'); //sitemap.xml for Google

// **************** Download from ftp the latest JArltech CSV
// set up basic connection
$conn_id = ftp_connect($ftp_server);

// login with username and password
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

// try to download $server_file and save to $local_file
if (ftp_get($conn_id, $local_file, $server_file, FTP_BINARY)) {
    echo "Successfully downloaded $local_file<br>";
} else {
    echo "There was a problem\n";
}

// close the connection
ftp_close($conn_id);

// **************** Remove the first line of the CSV

if ($handle = fopen("downloaded.csv", "c+")) {             // open the file in reading and editing mode
    if (flock($handle, LOCK_EX)) {               // lock the file, so no one can read or edit this file 
        while (($line = fgets($handle, 4096)) !== FALSE) { 
            if (!isset($write_position)) {        // move the line to previous position, except the first line
                $write_position = 0;
            } else {
                $read_position = ftell($handle); // get actual line
                fseek($handle, $write_position); // move to previous position
                fputs($handle, $line);           // put actual line in previous position
                fseek($handle, $read_position);  // return to actual position
                $write_position += strlen($line);    // set write position to the next loop
            }
        }
        fflush($handle);                         // write any pending change to file
        ftruncate($handle, $write_position);     // drop the repeated last line
        flock($handle, LOCK_UN);                 // unlock the file
    }
    fclose($handle);
	echo "First line removed!<br>";
}

// **************** Replace all commas "," with dots "." because of the price	
	
$path_to_file = 'downloaded.csv';
$file_contents = file_get_contents($path_to_file);
$file_contents = str_replace(",",".",$file_contents);
file_put_contents($path_to_file,$file_contents);
echo "Replaced all commas with dots!<br>";
echo "Updating prices and quantities!<br>";

// **************** Update prices and quantities in Database

// set local variables
$connect = mysql_connect($servername, $username, $password) or die('Could    not connect: ' . mysql_error());
$handle = fopen("downloaded.csv", "r");

// connect to mysql and select database or exit 
mysql_select_db($dbname, $connect);

$cnt_lines = $cnt_upd = 0;
// loop content of csv file, using comma as delimiter
while (($data = fgetcsv($handle, 1000, ';', '"')) !== false) {
	echo 'START LOOP ITEM: ';
	$product_name = '';
	$product_nr = $data[0];
	$category = $data[1];
	$sub_category = $data[2];
	$main_product = $data[3];
	$stock = $data[4];
	$packing_unit = $data[5];
	$product_name = sanitize_for_xml($data[6]);
	$product_description = sanitize_for_xml($data[7]);
	$datasheet = $data[8];
	$warranty = $data[9];
	$price =  $data[10];
	$Product_ID = $data[11];
	$picture = $data[12];
	$vendor = $data[13];
	$date_price_change = $data[15];
	$weight = $data[16];
	$pre_exchange = $data[18];
	$type = $data[19];
	$fullcover = $data[20];
	
	$cnt_lines++;
	echo '---->' . $product_name . '<----- ' . $Product_ID . " €" . $price . " (stock: " . $stock . ")<br>";
	
	$query = 'SELECT original_number FROM products';
	if (!$result = mysql_query($query)) {
		continue;
	} 
	
	if ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
	
		// entry exists update
		$query = "UPDATE products SET price ='$price' , quantity = '$stock' WHERE original_number = '$Product_ID' AND source = 'JAR'";
		
		
		mysql_query($query);
		if (mysql_affected_rows() <= 0) {
		 	echo ' no rows where affected by update query for productcode=' . $Product_ID;
		} else {
		    $cnt_upd++;
			echo ' >>>>PRICE / QTY UPDATED<<<< ';
		}
	} else {
		echo " entry for ".$Product_ID." doesn't exist<br>";	
	}
	
	mysql_free_result($result);
    echo 'The number of lines in the update file is '.$cnt_lines.', but only '.$cnt_upd. ' required update for stock and price';
    
	//************ XML START

	if ($price < 11) $xml_price = round($price * 1.6 * 1.2);
	elseif ($price > 10 AND $price < 51) $xml_price = round($price * 1.5 * 1.2, 2);
	elseif ($price > 50 AND $price < 101) $xml_price = round($price * 1.3 * 1.2, 2);
	elseif ($price > 100 AND $price < 201) $xml_price = round($price * 1.26 * 1.2);
	elseif ($price > 200 AND $price < 301) $xml_price = round($price * 1.24 * 1.2);
	elseif ($price > 300 AND $price < 501) $xml_price = round($price * 1.23 * 1.2);
	elseif ($price > 500 AND $price < 1000.01) $xml_price = round($price * 1.22 * 1.2);
	elseif ($price > 1000) $xml_price = round($price * 1.19 * 1.2);
	
	$url = 'https://tellimine.hool.ee/et/product/' . rawurlencode($main_product);
	
	//****** START we know English category name so we check from MySQL table 'categories' the Estonian name of the category
	$conn = new mysqli($servername, $username, $password, $dbname);
	$conn->set_charset("utf8mb4");	
	if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); } 
	mysqli_set_charset($con,"utf8");

	$sql = "SELECT name_et FROM categories WHERE name='$sub_category'";
	$result = $conn->query($sql);
	$row = $result->fetch_assoc();
	$cat_et = $row["name_et"];
	//****** END estonian names for categories
	
	if (strpos($product_name, '&') !== false) $add_xml = 0;
	else $add_xml = 1;
	
	if (!empty($product_name) AND $add_xml == 1 AND $xml_price > 0) { //add to XML if the product has name - DOESN'T WORK BY SOME REASON
		$track = $xml->addChild('Product');
		
		$track->addChild('Category', "$sub_category");
		$track->addChild('Vendor', "$vendor"); 
		$track->addChild('Name', "$product_name");
		$track->addChild('Code', "$Product_ID");
		$track->addChild('GTIN', "");
		$track->addChild('Description', "$product_description");
		$track->addChild('Price', "$xml_price");
		$track->addChild('InStore', "4-5tp");
		$track->addChild('URL', "$url");
		$track->addChild('AdditionalInfo', "Transport €6. Alates €450 ostust transport tasuta!");
		$track->addChild('Picture', "$picture");
		$track->addChild('ProductURL', "$datasheet");
		echo ' -ADDED to hinnavaatlus.xml- ';
	}
	else echo 'NO PRODUCT NAME OR PRICE<0 OR NAME CONTAINS &!<br>';
	
	//Start sitemap.xml for Google
	
	$today = date("Y-m-d");
	
	$Gurl = $Gxml->addChild('url'); 
	
	$Gurl->addChild('loc',$url.'/');
	$Gurl->addChild('lastmod',$today);
	
//************ XML FINISH

	echo ' END OF LOOP<br>';
}

fclose($handle);
mysql_close($connect);

//Save XML in hinnavaatlus.xml file
$myfile = fopen("hinnavaatlus.xml", "w") or die("Unable to open file!");
fwrite($myfile, $xml->asXML());
fclose($myfile);

//Save XML in sitemap.xml file
$myfile = fopen("sitemap.xml", "w") or die("Unable to open file!");
fwrite($myfile, $Gxml->asXML());
fclose($myfile);

function myUrlEncode($string) { //Generate URL from main product's name
    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
    return str_replace($entities, $replacements, urlencode($string));
}

$utf_8_range = range(0, 1114111);
$output = ords_to_utfstring($utf_8_range);
$sanitized = sanitize_for_xml($output);

/**
 * Removes invalid XML
 *
 * @access public
 * @param string $value
 * @return string
 */
function sanitize_for_xml($input) {
  // Convert input to UTF-8.
  $old_setting = ini_set('mbstring.substitute_character', '"none"');
  $input = mb_convert_encoding($input, 'UTF-8', 'auto');
  ini_set('mbstring.substitute_character', $old_setting);

  // Use fast preg_replace. If failure, use slower chr => int => chr conversion.
  $output = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', '', $input);
  if (is_null($output)) {
    // Convert to ints.
    // Convert ints back into a string.
    $output = ords_to_utfstring(utfstring_to_ords($input), TRUE);
  }
  return $output;
}

/**
 * Given a UTF-8 string, output an array of ordinal values.
 *
 * @param string $input
 *   UTF-8 string.
 * @param string $encoding
 *   Defaults to UTF-8.
 *
 * @return array
 *   Array of ordinal values representing the input string.
 */
function utfstring_to_ords($input, $encoding = 'UTF-8'){
  // Turn a string of unicode characters into UCS-4BE, which is a Unicode
  // encoding that stores each character as a 4 byte integer. This accounts for
  // the "UCS-4"; the "BE" prefix indicates that the integers are stored in
  // big-endian order. The reason for this encoding is that each character is a
  // fixed size, making iterating over the string simpler.
  $input = mb_convert_encoding($input, "UCS-4BE", $encoding);

  // Visit each unicode character.
  $ords = array();
  for ($i = 0; $i < mb_strlen($input, "UCS-4BE"); $i++) {
    // Now we have 4 bytes. Find their total numeric value.
    $s2 = mb_substr($input, $i, 1, "UCS-4BE");
    $val = unpack("N", $s2);
    $ords[] = $val[1];
  }
  return $ords;
}

/**
 * Given an array of ints representing Unicode chars, outputs a UTF-8 string.
 *
 * @param array $ords
 *   Array of integers representing Unicode characters.
 * @param bool $scrub_XML
 *   Set to TRUE to remove non valid XML characters.
 *
 * @return string
 *   UTF-8 String.
 */
function ords_to_utfstring($ords, $scrub_XML = FALSE) {
  $output = '';
  foreach ($ords as $ord) {
    // 0: Negative numbers.
    // 55296 - 57343: Surrogate Range.
    // 65279: BOM (byte order mark).
    // 1114111: Out of range.
    if (   $ord < 0
        || ($ord >= 0xD800 && $ord <= 0xDFFF)
        || $ord == 0xFEFF
        || $ord > 0x10ffff) {
      // Skip non valid UTF-8 values.
      continue;
    }
    // 9: Anything Below 9.
    // 11: Vertical Tab.
    // 12: Form Feed.
    // 14-31: Unprintable control codes.
    // 65534, 65535: Unicode noncharacters.
    elseif ($scrub_XML && (
               $ord < 0x9
            || $ord == 0xB
            || $ord == 0xC
            || ($ord > 0xD && $ord < 0x20)
            || $ord == 0xFFFE
            || $ord == 0xFFFF
            )) {
      // Skip non valid XML values.
      continue;
    }
    // 127: 1 Byte char.
    elseif ( $ord <= 0x007f) {
      $output .= chr($ord);
      continue;
    }
    // 2047: 2 Byte char.
    elseif ($ord <= 0x07ff) {
      $output .= chr(0xc0 | ($ord >> 6));
      $output .= chr(0x80 | ($ord & 0x003f));
      continue;
    }
    // 65535: 3 Byte char.
    elseif ($ord <= 0xffff) {
      $output .= chr(0xe0 | ($ord >> 12));
      $output .= chr(0x80 | (($ord >> 6) & 0x003f));
      $output .= chr(0x80 | ($ord & 0x003f));
      continue;
    }
    // 1114111: 4 Byte char.
    elseif ($ord <= 0x10ffff) {
      $output .= chr(0xf0 | ($ord >> 18));
      $output .= chr(0x80 | (($ord >> 12) & 0x3f));
      $output .= chr(0x80 | (($ord >> 6) & 0x3f));
      $output .= chr(0x80 | ($ord & 0x3f));
      continue;
    }
  }
  return $output;
}
?>
</body>
</html>